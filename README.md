# qiankun集成说明文档
### 概述
此文档主要为了各个系统的前端能够借以微前端的形式使用qiankun框架接入其他系统，或者嵌入到其他系统，或者单个系统的前端将各个业务模块做拆分，分解成一些更小、更简单的能够独立开发、测试、部署的小块。基于以上需求作出集成操作说明。
 qiankun 官方文档见：[https://qiankun.umijs.org/zh/guide/getting-started][1] 
### 主应用
1. 安装 qiankun
	$ yarn add qiankun # 或者 npm i qiankun -S
2. 在主应用中注册微应用
	```注册子应用
	import { registerMicroApps, start } from 'qiankun';
	registerMicroApps([
	  {
		  name: 'service_activity_app', // app name registered
		  entry: process.env.NODE_ENV == 'production' ? 'https://taiping.fulan.com.cn/dist_test_hash/vue.html' : 'http://localhost:9001',
		  container: '#subContainer',
		  activeRule: (location) => {
	    	return location.href.includes('/microApp/serviceActivityApp')
		  }
		},
	]);
	start();
	```
- name：必选，微应用的名称，与微应用package.json中name属性值一致，微应用之间必须确保唯一；
- entry：必选，微应用的入口访问地址，需要区分本地调试环境还是线上环境；
- container：必选，微应用的容器节点的选择器或者 Element 实例，一般是主应用提供的用于承载微应用的容器节点。如`container: '#subContainer' `或` container: document.querySelector('#subContainer')`。
- activeRule：`string | (location: Location) => boolean | Array<string | (location: Location) => boolean` - 必选，微应用的激活规则。
	- 支持直接配置字符串或字符串数组，如 `_activeRule: '/app1' _`或 `activeRule:  activeRule: ['/app1', '/app2']`当配置为字符串时会直接跟 url 中的路径部分做前缀匹配，匹配成功表明当前应用会被激活
	- 支持配置一个 active function 函数或一组 active function。函数会传入当前 location 作为参数，函数返回 true 时表明当前微应用会被激活。如 `location => location.pathname.startsWith('/app1')`

### 微应用
1. 导出相应的生命周期钩子
	微应用需要在自己的入口 js (通常就是你配置的 webpack 的 entry js) 导出 bootstrap、mount、unmount 三个生命周期钩子，以供主应用在适当的时机调用。
	```微应用
	let VueApp = null
	let routers = null
	const __qiankun__ = window.__POWERED_BY_QIANKUN__

	function render(props = {}) {
	  routers = router

	  VueApp = new Vue({
		el: '#app',
		router,
		store,
		template: '<App/>',
		components: {
		  App,
		},
	  })

	  window.VueApp = VueApp
	}

	if (!__qiankun__) {
	  render()
	}

	/**
	 * bootstrap 只会在微应用初始化的时候调用一次，下次微应用重新进入时会直接调用 mount 钩子，不会再重复触发 bootstrap。
	 * 通常我们可以在这里做一些全局变量的初始化，比如不会在 unmount 阶段被销毁的应用级别的缓存等。
	 */
	export async function bootstrap({ actions, state, pager } = {}) {
	}
	/**
	 * 应用每次进入都会调用 mount 方法，通常我们在这里触发应用的渲染方法
	 */
	export async function mount(props = {}) {
	  render(props)
	}
	/**
	 * 应用每次 切出/卸载 会调用的方法，通常在这里我们会卸载微应用的应用实例
	 */
	export async function unmount() {
	  VueApp.$destroy()
	  VueApp.$el.innerHTML = ''
	  VueApp = null
	  routers = null
	}
	/**
	 * 可选生命周期钩子，仅使用 loadMicroApp 方式加载微应用时生效
	 */
	export async function update(props) {
	  console.log('update props', props)
	}
	```
2.  配置微应用的打包工具
	除了代码中暴露出相应的生命周期钩子之外，为了让主应用能正确识别微应用暴露出来的一些信息，微应用的打包工具需要增加如下配置：

	**webpack:**[^1]

	```修改webpack配置
	const packageName = require('./package.json').name;
	module.exports = {
	  output: {
		library: `${packageName}-[name]`,
		libraryTarget: 'umd',
		jsonpFunction: `webpackJsonp_${packageName}`,
	  },
	};
	```

### 路由
#### 主应用是 hash 模式
当主应用是 hash 模式时，一般微应用也是 hash 模式。主应用的一级 hash 路径会分配给对应的微应用（比如 #/base1 ），此时微应用如果需要在 base 路径的基础上进行 hash 模式下的二级路径跳转（比如 #/base1/child1 ），这个场景在当前 VueRouter 的实现方式下需要自己手动实现，给所有路由都添加一个前缀

```修改路由配置
const __qiankun__ = window.__POWERED_BY_QIANKUN__
let prefix = '/'

if (__qiankun__) {
  prefix = '/microApp/serviceActivityApp/'
}

let saname
Vue.use(Router)
const router = new Router({
  routes: [
    {
      path: prefix === '/' ? prefix : prefix.substring(0, prefix.length - 1),
      redirect: prefix + 'layout/callback',
    },
    {
      name: 'layout',
      path: prefix + 'layout',
      component(resolve) {
        require.ensure(
          [],
          () => resolve(require('../components/common/Layout.vue')),
          'layout'
        )
      },
    }
   ],
})
```


**重写微应用路由跳转方法，兼容qiankun/非qiankun环境页面跳转**

```修改路由配置
//qinakun环境下需要对push进行重写
if (__qiankun__) {
  const originalPush = Router.prototype.push;
  Router.prototype.push = function push(location) {
    let path = "";
    let isStringLocation = false;
    if (Common.isObject(location)) {
      path = location.path;
    } else if (Common.isString(location)) {
      isStringLocation = true;
      path = location;
    }

    //跳转到其他系统
    if (isStringLocation) {
      location = pushPrefix + path;
    } else {
      location.path = pushPrefix + path;
    }
    return originalPush.call(this, location);
  };
}
```

**主应用跳转微应用**（以vue为例）

`this.$router.push({ path: '/microApp/serviceActivityApp/layout/serviceActivity/serviceActivityList'})`
或
`this.$router.push('/microApp/serviceActivityApp/layout/serviceActivity/serviceActivityList')`

**微应用内页面跳转** （以vue为例）

`this.$router.push({ path: '/layout/manualGuide/manualGuideHome'})`
或
`this.$router.push('/layout/manualGuide/manualGuideHome')`

#### 主应用是 history 模式
- 当主应用是 history 模式且微应用也是 history 模式时，表现完美。如果微应用需要添加 base 路径，设置子项目的 base 属性即可。
	```修改路由配置
	const router = new Router({
	  base: '/microApp/serviceActivityApp/',
	  routes: [
		{
		  path: '/',
		  redirect: 'layout/callback',
		}
	  ],
	})
	```
- 当主应用是 history 模式，微应用是 hash 模式，无需修改  

### 应用之间数据共享

使用**initGlobalState**

**主应用**
```数据共享
//Main.js
//主应用注册通信方式
let state = {
  from: '主应用',
  data: { message: '主应用的消息' },
}
const microAppStateActions = initGlobalState(state)

//如果这里共享的数据使用vuex store存储， props可以传入store
const props = {
  state: microAppStateActions,
}

//注册共享数据监听
microAppStateActions &&
  microAppStateActions.onGlobalStateChange((data) => {
    alert(`----- 主应用收到${data.from}发来的消息`)
  })

//修改共享数据
microAppStateActions.setGlobalState({
    from: '主应用',
    data: { message: '主应用定时发送的消息' },
  })

registerMicroApps(
  [
    {
      name: 'service_activity_app', // app name registered
      entry:
        process.env.NODE_ENV == 'production'
          ? 'https://taiping.fulan.com.cn/dist_test_hash/vue.html'
          : 'http://localhost:9001',
      container: '#subContainer',
      activeRule: (location) => {
        return location.href.includes('/microApp/serviceActivityApp')
      },
      props,
    }
  ]
)
```

**微应用**
```数据共享
//Main.js
export async function bootstrap({ state } = {}) {
  if (state) {
    console.log('------- taiping state', state)
    Vue.prototype.$state = state

	//注册共享数据监听
	state && state((data) => {
	  alert(`----- 微应用收到${data.from}发来的消息`)
	})
  }
}

//其他页面 修改共享数据
Vue.prototype.$state.setGlobalState({
    from: '微应用',
    data: { message: '微应用发送的消息' },
  })
```

### App启动本地web服务

`App启动本地web服务`主要是针对App对基于qiankun微前端框架的离线资源加载，由于qiankun获取微应用资源基于HTTPserver，故需要在App内部启动一个web服务。
以iOS为例，这里是使用cocoapod管理第三方依赖库。

![Podfile添加CocoaHTTPServer][image-1]

**引入CocoaHTTPServer**
`#import <HTTPServer.h>`
**启动服务**
```启动服务
- (void)openServer{
	self.httpServer=[[HTTPServer alloc]init];
	[self.httpServer setType:@"_http._tcp."];
	[self.httpServer setPort:8080];
	NSString *webPath = [self getLocalServerPath];
	[self.httpServer setDocumentRoot:webPath];
	NSLog(@"服务器路径：%@", webPath);
	_startServer = [self startServer];
}

- (BOOL)startServer{
	BOOL ret = NO;
	NSError *error = nil;
	if( [self.httpServer start:&error]){
    	NSLog(@"HTTP服务器启动成功端口号为： %hu", [_httpServer listeningPort]);
    	self.serverPort = [NSString stringWithFormat:@"%d",[self.httpServer listeningPort]];
    	ret = YES;
	}else{
    	NSLog(@"启动HTTP服务器出错: %@", error);
	}
	return ret;
}

- (void)stopServer{//停止服务
	if (self.httpServer != nil){
    	[self.httpServer stop];
    	_startServer = NO;
	}
}

```
**webView加载本地H5资源**
```加载本地服务
NSString *str = [NSString stringWithFormat:@"http://localhost:8080/vue.html"];
NSURL *url = [NSURL URLWithString:str];
NSURLRequest *request = [NSURLRequest requestWithURL:url];
[self.webView loadRequest:request];
```


[^1]:	无 webpack 等构建工具的应用接入方式请见：https://qiankun.umijs.org/zh/faq#非-webpack-构建的微应用支持接入-qiankun-么？

[1]:	https://qiankun.umijs.org/zh/guide/getting-started

[image-1]:	http://localhost:3000/DraggedImage.png